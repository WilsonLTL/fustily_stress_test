let config = {};
// For setup the server, please modify the REDIS_URL link to the target redis
config.LOGGER_STATUS = true;
config.NS_DEFAULTPORT = 3000;
config.POSTID = "POSTID";
config.REDIS_PORT = 6379;
config.REDIS_URL = "139.162.46.131";
// For POSTNUM and USERNUM, since which isn't know how many user and post are exist (No API for that), so just generate a limit,
// It should be modify and NO HARDCODE if which is using in production.
config.POSTNUM = 10000;
config.USERNUM = 10000;
module.exports = config;