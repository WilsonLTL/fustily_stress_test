# Fastify Voting System

## Target and explain my design choices
A Voting system base on Fastify, Redis, Docker, Docker Compose and Nginx <br >
Detail of this system and how did I develop it: https://www.youtube.com/watch?v=K11VvMdtW7Q <br >
My Target For this system: <br >
1. Easy Scale Up <br >
2. Handle Large Size Request <br >
3. Well Documentation <br >
** Two show case in the server<br >
** For scale up show case, the docker compose was in Port:8888 <br >
** For stress test case, please send request in Port:8080 if you really need to do that<br >

## Solution 
### a) If we have a good main server
![img](https://i.imgur.com/Ar67Ids.png)
### b) If we have many many server
![img](https://i.imgur.com/0NDObuA.png)

### Local File tree
```.
├── Dockerfile
├── README.md
├── config.js 
├── docker-compose.yml
├── mocha
│   ├── ci_test.js
│   └── generate_data.js
├── package-lock.json
├── package.json
├── schemas.js
└── server.js
```

### Docker Compose tree
```
├── Nginx
├── Node Server1
├── Node Server2
└── Scalable Node Server ...
```

## 0. Prepare in your main server
```
1. sudo apt-get update -qy
2. sudo apt install npm -qy
3. sudo apt install docker.io -qy
4. sudo apt install docker-compose -qy
5. docker run -d --name redis -p 6379:6379 redis
```

## 1. Setup the main node server - Ubuntu
Tutorial video: https://youtu.be/xzs0QvNNiks 
```
1. sudo apt-get update -qy
2. sudo apt install npm -qy
3. sudo apt install docker.io -qy
4. sudo apt install docker-compose -qy
5. sudo docker run -d --name redis -p 6379:6379 redis
-------------It should be done in ------------
6. git clone https://gitlab.com/WilsonLTL/fustily_stress_test.git
7. sudo docker-compose up -d
8. sudo docker exec -it NGINX_CONTAINER_ID /bin/bash
9. apt-get update && apt-get install vim -qy
10. cd /etc/nginx/conf.d
11. vim default.conf
12. Modify all the code in to ##Nginx conf.d
13. sudo docker restart NGINX_CONTAINER_ID
PS1 :##Nginx conf.d in Reference doc
PS2 : Modify the config.js ()
PS3: In Nginx conf.d, cat /etc/hosts and echo ${PORT} to find host and port
```

## 2. API Guide
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/326da66d6019e507c6e8)<br >
API Guide Video: https://www.youtube.com/watch?v=-r-Rs8RYb5g <br >
Url : GET:/vote/$SCORE/$USERID/$POSTID <br >
e.g : http://139.162.46.131:8888/vote/1/1/1 <br >
Time Cost : 5ms to response/local - 8ms to response/Non-local(loadimpact)  <br >
$SCORE : integer <br >
$USERID : integer <br >
$POSTID : integer <br >
```
{
    "success": true/false
}
```

Url : GET:vote-count/$POSTID <br >
e.g :  http://139.162.46.131:8888/vote-count/1  <br >
Time Cost : 3ms to response/local - 6ms to response/Non-local(loadimpact)  <br >
$POSTID : integer <br >
```
{
    "upvote": integer,
    "downvote": integer
}
```

Url : GET:/vote-status/$USERID <br >
e.g http://139.162.46.131:8888/vote-status/1 <br >
Time Cost : 3ms to response/local - 7ms to response/Non-local(loadimpact)  <br >
$USERID : integer <br >
```
[
    {
        "postid":integer ,
        "score": integer
    },
    ...
]
```

## 3. API Stress test report
** Since the server is not good enough to handle a complex docker compose(only have 2 cpus), so I start a node server in 8080 to stress test the API <br > 
Node method:Pm2 start server.js -i max <br >
Stress Test API Port:8080 <br >
Stress Test Tool: [loader.io](https://loader.io) <br >
Read Test: https://www.youtube.com/watch?v=bHGYqXqOsfg <br >
Test Report: http://bit.ly/2V2u7rX <br >
![img](https://i.imgur.com/iVsAzpc.png)


Write Test: https://www.youtube.com/watch?v=g_GXGQxziIo <br >
Test Report: http://bit.ly/2Yy0gtH <br >
![img](https://i.imgur.com/zvvAtZB.png)


## 4. Scale up solution
To scale up to 100 node, we will using Nginx to do the load balance:
```
1. Get the new node host and port of running server.js
2. Modify the Nginx conf.d upstream node_server
e.g server 31.24.67.131:8080
3. Restart the Nginx container
```

### Docker node
```
1. Deploy the new node and run the docker image
2. Modify the main server Nginx conf.d setting
3. Restart the Nginx
```
### Nodejs Node
```
1. Deploy the code and using pm2 to start the service
2. Modify the main server Nginx conf.d setting
3. Restart the Nginx
```
### Cloud service
```
1. Do same things in Docker node or Nodejs Node
2. Modify the main server Nginx conf.d setting
3. Restart the Nginx
```

## 5. Limit of develop this assignment
 https://www.youtube.com/watch?v=eDVjJHWOQVc
1. Lack experience to handle large size data request like that (3K+/10K+)
2. Short period to learn a new framework and tools
3. Time (just get two day to do it since I still need to work and study)

## Reference doc

### Ubuntu - update nodejs
```
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
```

### Nginx conf.d
** If we got a lots of CPU, we can add worker_processes:YOU_CPU_NUM*2 in to the conf.d <br >
** Increase client_max_body_size and worker_connections to handle more REQUEST !!! 
** For that ^ , modify /etc/nginx/nginx.conf, not /etc/nginx/conf.d/default.conf 
```ginx
upstream node_server {
   server 172.25.0.3:8081;
   server 172.25.0.2:8080;
}
server {
   listen 80;
   server_name localhost;
   location / {
     proxy_pass http://node_server/; 
   }
}
```
```
worker_processes 4;
events { worker_connections: 10240; }
http {
    ...
}
```
