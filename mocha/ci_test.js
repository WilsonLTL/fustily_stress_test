const supertest = require('supertest');
const { expect } = require('chai');
const api = supertest('http://54.254.149.198:8888');

//connection testing
before((done) => {
    api.get('/')
        .expect(200)
        .end((e,res) => {
            console.log("Connection test successful");
            done()
        })
});

describe('API vet check', () => {
    // Vote api check
    it('Vote API check', (done) => {
        // score = 1, userid = 1, postid =1
        api.get('/vote/1/1/1')
            .expect(200)
            .end((e,res)=> {
                done();
            });
    });

    // Vote count api check
    it('Vote-count check', (done) => {
        // postid =1
        api.get('/vote-count/1')
            .expect(200)
            .end((e,res)=> {
                done()
            });
    });

    //Vote status api check
    it('Vote-status check', (done) => {
        // userid =1
        api.get('/vote-status/1')
            .expect(500)
            .end((e, res) => {
                done()
            });
    });
});