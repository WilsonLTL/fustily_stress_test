const supertest = require('supertest');
const api = supertest('http://52.221.183.116:8888');

//connection testing
before((done) => {
    api.get('/')
        .expect(200)
        .end((e,res) => {
            console.log("Connection test successful");
            done()
        })
});

describe('Generate simple data', () => {
    it('Generate vote data', (done) => {
        for (let i=1;i<=100;i++){
            for (let n=1;n<=100;n++){
                let random_score = Math.random() < 0.5 ? -1 : 1;
                // api: vote/userid/postid
                api.get('/vote/'+random_score+'/'+i+'/'+n).end((e,res) => {});
            }
        }
        done();
    });
    it('Get vote count', (done) => {
        for (let i=1;i<=100;i++){
            api.get('/vote-count/'+i)
                .end((e,res) => {});
        }
        // console.log(vote_count_list);
        done();
    });
    it('Get vote status', (done) => {
        for (let i=1;i<=100;i++){
            api.get('/vote-status/'+i)
                .end((e,res) => {});
        }
        done();
    });
});