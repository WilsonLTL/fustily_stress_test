FROM node:latest
RUN mkdir /dir
WORKDIR /dir
ADD . /dir
RUN npm install
CMD ["node","server.js"]
