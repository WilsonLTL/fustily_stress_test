//Schema for vote API
const vote_schema = {
    schema: {
        params: {
            type: 'object',
            required:['score','userid','postid'],
            properties: {
                score: {type: 'integer'},
                userid: {type: 'integer'},
                postid: {type: 'integer'},
            },
            additionalProperties: false
        },
        response: {
            200: {
                type: 'object',
                properties: {
                    success: {type: 'boolean'}
                }
            }
        }
    }
};

//Schema for vote-count API
const vote_count_schema = {
    schema: {
        params: {
            type: 'object',
            required:['postid'],
            properties: {
                postid: {type: 'integer'},
            },
            additionalProperties: false
        },
        response: {
            200: {
                type: 'object',
                properties: {
                    upvote: {type: 'integer'},
                    downvote: {type: 'integer'}
                }
            }
        }
    }
};

//Schma for vote-status API
const vote_status_schema = {
    schema: {
        params: {
            type: 'object',
            required:['userid'],
            properties: {
                userid: {type: 'integer'},
            },
            additionalProperties: false
        }
    }
};

module.exports = {
    vote_schema,vote_count_schema,vote_status_schema
};