const config = require('./config');
const fastify = require('fastify')({logger: config.LOGGER_STATUS});
const redis = require('redis');
const schedule = require("node-schedule");
const { vote_schema,vote_count_schema,vote_status_schema } = require('./schemas');
const redis_se = redis.createClient({port:config.REDIS_PORT,host:config.REDIS_URL});
const port = process.env.PORT || config.NS_DEFAULTPORT;
let redis_result = [],user_result = [];
redis_result.length = config.POSTNUM;
user_result.length = config.USERNUM;
// this is a sepecial value, which should be refer to how many post is exist in the real case


// Redis Coonect Checking
redis_se.on('connect', () => {
    console.log('Redis client connection successful');
});

// Route setting
fastify
    .get("/", enter_api_handler)
    .get("/loaderio-6891849c95e859d0951e9d3ec4907a9d/", verification_handler)
    .get("/vote/:score/:userid/:postid", vote_schema,vote_handler)
    .get("/vote-count/:postid", vote_count_schema,vote_count_hander)
    .get("/vote-status/:userid",vote_status_schema, vote_status_hander);

// Just say HI
async function enter_api_handler(req, res) {
    res.send({ status: true })
}

// For loader.io
async function verification_handler(req, res) {
    res.send("loaderio-6891849c95e859d0951e9d3ec4907a9d")
}

// Vote API
async function vote_handler(req, res) {
    try{
        return new Promise(async function(){
            //params : score,userid,postid
            //upvote & downvote, vote record
            parseInt(req.params.score) === 1 ? redis_se.incr(config.POSTID+parseInt(req.params.postid)+"_UPVOTE") : redis_se.incr(config.POSTID+parseInt(req.params.postid)+"_DOWNVOTE");
            redis_se.lpush(parseInt(req.params.userid),JSON.stringify({"postid" : parseInt(req.params.postid),"score" : parseInt(req.params.score)}));
            return res.send({success:true})
        })
    }
    catch (e) {
        res.code(500);
        await res.send({success:false,err:e})
    }
}

//Vote count API
async function vote_count_hander(req, res) {
    try{
        //params : postid
        // get the total count of the post, data will be update per min
        let result = (redis_result[req.params.postid] == null ? {"upvote":0,"downvote":0}:{"upvote":redis_result[req.params.postid-1].split(":")[0].split("-")[1],"downvote":redis_result[req.params.postid-1].split(":")[1].split("-")[1]});
        res.send(result);
    }catch (e){
        res.code(500);
        return res.send({success:false,err:e})
    }
}

// Vote status API
async function vote_status_hander(req, res){
    try{
        // params : userid
        // get the recent vote of user, data will be update per min
        let result = (redis_result[req.params.userid] == null ? []:user_result[req.params.userid-1]);
        res.send(result);
    }catch (e){
        res.code(500);
        return res.send({success:false,err:e})
    }
}

// Scheduler to update data
let rule = new schedule.RecurrenceRule();
rule.second = 1;
let schedule_mission = schedule.scheduleJob(rule, async function(){
    //Update the user data and post data
    let i = 1,n =1;
    while(i <= config.POSTNUM){
        await update_post_data(i);
        await update_user_data(i);
        i++;
    }
});

let update_post_data = (id) => {
    let upvote,downvote;
    // update the posts upvote and downvote
    redis_se.get(config.POSTID+id+"_UPVOTE",function(err,value){
        value != null ? upvote = value : upvote = 0;
        redis_se.get(config.POSTID+id+"_DOWNVOTE",function (err,value) {
            value != null ? downvote = value : downvote = 0;
            redis_result[id-1] = "upvote-"+upvote+":downvote-"+downvote;
        });
    });
};

let update_user_data = (id) => {
    // update the user recent votes
    redis_se.lrange(id, 0, 99 ,function (err,value) {
        user_result[id-1] = JSON.parse('['+value+']');
    });
};

//Start the server
fastify.listen(port,'0.0.0.0', (err, address) => {
    if (err) throw err
    fastify.log.info(`server listening on` + address)
});